package it.stachura.vfc.server.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import it.stachura.vfc.server.documents.Hello;
import it.stachura.vfc.server.service.impl.HelloServiceImpl;

@CrossOrigin
@RestController
public class HelloWorldControllerRest {
	
	@Autowired
	HelloServiceImpl helloService;

	@RequestMapping(value = "/hello",  method = RequestMethod.GET)
	public Hello helolo() {
		return helloService.getHello();
	}

}
