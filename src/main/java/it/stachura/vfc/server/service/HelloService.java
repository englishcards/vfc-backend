package it.stachura.vfc.server.service;

import it.stachura.vfc.server.documents.Hello;

public interface HelloService {
	Hello getHello();
}
